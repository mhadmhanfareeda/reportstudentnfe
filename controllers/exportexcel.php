<?php
include "../configs/connection.php";
include "../PHPExcel/Classes/PHPExcel.php";
include "../controllers/querystudent.class.php";
include "../controllers/queryclassstudenttallthailand.class.php";

$connect        = OpenCon();
$obj            = new Query();
$objAll         = new QueryAll();
$type           = $_GET['type'];
$year           = $_GET['year'];
$term           = $_GET['term'];
$districtuid    = $_GET['districtuid'];
$subdistrictuid = $_GET['subdistrictuid'];
$provinceuid    = $_GET['provinceuid'];



if ($type == 'Allthailand' && $type!=null)
{
    $dataclass11 = $objAll->queryClassStudent11($connect, $year, $term);
    $objclass11  = json_decode($dataclass11);

    $dataclass12 = $objAll->queryClassStudent12($connect, $year,  $term);
    $objclass12  = json_decode($dataclass12);

    $dataclass13 = $objAll->queryClassStudent13($connect, $year, $term);
    $objclass13  = json_decode($dataclass13);

    $totalAll = $objclass11->{'totalfinalclass11'}+$objclass12->{'totalfinalclass12'}+$objclass13->{'totalfinalclass13'};

    $provinceName = 'ทั้งประเทศไทย';
}else{
    $dataclass11 = $obj->queryClassStudent11($provinceuid, $connect, $year, $term, $districtuid, $subdistrictuid);
    $objclass11  = json_decode($dataclass11);
    
    $dataclass12 = $obj->queryClassStudent12($provinceuid, $connect, $year, $term, $districtuid, $subdistrictuid);
    $objclass12  = json_decode($dataclass12);
    
    $dataclass13 = $obj->queryClassStudent13($provinceuid, $connect, $year, $term, $districtuid, $subdistrictuid);
    $objclass13  = json_decode($dataclass13);
    
    $totalAll = $objclass11->{'totalfinalclass11'}+$objclass12->{'totalfinalclass12'}+$objclass13->{'totalfinalclass13'};

    if($_GET['province_name']){
        $provinceName = $_GET['province_name'];
    }else if($_GET['subdistrictname']){
        $provinceName = $_GET['subdistrictname'];
    }else if($_GET['districtname']){
        $provinceName = $_GET['districtname'];
    }else if($_GET['provincename']){
        $provinceName = $_GET['provincename'];
    }
}

$excel = new PHPExcel();

// insert someData
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
);

$excel->getDefaultStyle()->applyFromArray($style);

$excel->getActiveSheet()
    ->getStyle('B4:G27')
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

$excel->getActiveSheet()
    ->getStyle('A2:F2')
    ->getFill()
    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()
    ->setRGB('4DC46F');

$excel->getActiveSheet()
    ->getStyle('B3:G3')
    ->getFill()
    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()
    ->setRGB('4DC46F');

$excel->getActiveSheet()
    ->getStyle('A25:A27')
    ->getFill()
    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()
    ->setRGB('4DC46F');

$excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
$excel->getDefaultStyle()->getFont('A1:G22')->setName('Arial');
$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);

$excel->getActiveSheet()->getStyle('A2:G27')->applyFromArray($styleArray);
$excel->getActiveSheet()->setTitle("ReportStudent");

$excel->setActiveSheetIndex(0)
    ->mergeCells('B2:C2')
    ->mergeCells('D2:E2')
    ->mergeCells('F2:G2')
    ->mergeCells('A2:A3')
    ->mergeCells('A1:G1')
    ->setCellValue('A1', "รายงานข้อมูลนักศึกษาของสำนักงาน กศน.$provinceName จำแนกตามช่วงอายุ")
    ->setCellValue('A2', 'ช่วงอายุ')
    ->setCellValue('A4', '<12')
    ->setCellValue('A5', '12')
    ->setCellValue('A6', '13')
    ->setCellValue('A7', '14')
    ->setCellValue('A8', '15')
    ->setCellValue('A9', '16')
    ->setCellValue('A10', '17')
    ->setCellValue('A11', '18')
    ->setCellValue('A12', '19')
    ->setCellValue('A13', '20')
    ->setCellValue('A14', '21')
    ->setCellValue('A15', '22')
    ->setCellValue('A16', '23')
    ->setCellValue('A17', '24')
    ->setCellValue('A18', '25-29')
    ->setCellValue('A19', '30-34')
    ->setCellValue('A20', '35-39')
    ->setCellValue('A21', '40-44')
    ->setCellValue('A22', '45-49')
    ->setCellValue('A23', '50-60')

    ->setCellValue('B2', 'ประถมศึกษา')
    ->setCellValue('B3', 'ชาย')
    ->setCellValue('C3', 'หญิง')
    ->setCellValue('B4', number_format($objclass11->{'agelessMale'}))
    ->setCellValue('C4', number_format($objclass11->{'agelessFemale'}))
    ->setCellValue('B5', number_format($objclass11->{'equal12Male'}))
    ->setCellValue('C5', number_format($objclass11->{'equal12Female'}))
    ->setCellValue('B6', number_format($objclass11->{'equal13Male'}))
    ->setCellValue('C6', number_format($objclass11->{'equal13Female'}))
    ->setCellValue('B7', number_format($objclass11->{'equal14Male'}))
    ->setCellValue('C7', number_format($objclass11->{'equal14Female'}))
    ->setCellValue('B8', number_format($objclass11->{'equal15Male'}))
    ->setCellValue('C8', number_format($objclass11->{'equal15Female'}))
    ->setCellValue('B9', number_format($objclass11->{'equal16Male'}))
    ->setCellValue('C9', number_format($objclass11->{'equal16Female'}))
    ->setCellValue('B10', number_format($objclass11->{'equal17Male'}))
    ->setCellValue('C10', number_format($objclass11->{'equal17Female'}))
    ->setCellValue('B11', number_format($objclass11->{'equal18Male'}))
    ->setCellValue('C11', number_format($objclass11->{'equal18Female'}))
    ->setCellValue('B12', number_format($objclass11->{'equal19Male'}))
    ->setCellValue('C12', number_format($objclass11->{'equal19Female'}))
    ->setCellValue('B13', number_format($objclass11->{'equal20Male'}))
    ->setCellValue('C13', number_format($objclass11->{'equal20Female'}))
    ->setCellValue('B14', number_format($objclass11->{'equal21Male'}))
    ->setCellValue('C14', number_format($objclass11->{'equal21Female'}))
    ->setCellValue('B15', number_format($objclass11->{'equal22Male'}))
    ->setCellValue('C15', number_format($objclass11->{'equal22Female'}))
    ->setCellValue('B16', number_format($objclass11->{'equal23Male'}))
    ->setCellValue('C16', number_format($objclass11->{'equal23Female'}))
    ->setCellValue('B17', number_format($objclass11->{'equal24Male'}))
    ->setCellValue('C17', number_format($objclass11->{'equal24Female'}))
    ->setCellValue('B18', number_format($objclass11->{'25to29Male'}))
    ->setCellValue('C18', number_format($objclass11->{'25to29Female'}))
    ->setCellValue('B19', number_format($objclass11->{'30to34Male'}))
    ->setCellValue('C19', number_format($objclass11->{'30to34Female'}))
    ->setCellValue('B20', number_format($objclass11->{'35to39Male'}))
    ->setCellValue('C20', number_format($objclass11->{'35to39Female'}))
    ->setCellValue('B21', number_format($objclass11->{'40to44Male'}))
    ->setCellValue('C21', number_format($objclass11->{'40to44Female'}))
    ->setCellValue('B22', number_format($objclass11->{'45to49Male'}))
    ->setCellValue('C22', number_format($objclass11->{'45to49Female'}))
    ->setCellValue('B23', number_format($objclass11->{'more50Male'}))
    ->setCellValue('C23', number_format($objclass11->{'more50Female'}))

    ->setCellValue('D2', 'ม.ต้น')
    ->setCellValue('D3', 'ชาย')
    ->setCellValue('E3', 'หญิง')
    ->setCellValue('D4', number_format($objclass12->{'agelessMale'}))
    ->setCellValue('E4', number_format($objclass12->{'agelessFemale'}))
    ->setCellValue('D5', number_format($objclass12->{'equal12Male'}))
    ->setCellValue('E5', number_format($objclass12->{'equal12Female'}))
    ->setCellValue('D6', number_format($objclass12->{'equal13Male'}))
    ->setCellValue('E6', number_format($objclass12->{'equal13Female'}))
    ->setCellValue('D7', number_format($objclass12->{'equal14Male'}))
    ->setCellValue('E7', number_format($objclass12->{'equal14Female'}))
    ->setCellValue('D8', number_format($objclass12->{'equal15Male'}))
    ->setCellValue('E8', number_format($objclass12->{'equal15Female'}))
    ->setCellValue('D9', number_format($objclass12->{'equal16Male'}))
    ->setCellValue('E9', number_format($objclass12->{'equal16Female'}))
    ->setCellValue('D10', number_format($objclass12->{'equal17Male'}))
    ->setCellValue('E10', number_format($objclass12->{'equal17Female'}))
    ->setCellValue('D11', number_format($objclass12->{'equal18Male'}))
    ->setCellValue('E11', number_format($objclass12->{'equal18Female'}))
    ->setCellValue('D12', number_format($objclass12->{'equal19Male'}))
    ->setCellValue('E12', number_format($objclass12->{'equal19Female'}))
    ->setCellValue('D13', number_format($objclass12->{'equal20Male'}))
    ->setCellValue('E13', number_format($objclass12->{'equal20Female'}))
    ->setCellValue('D14', number_format($objclass12->{'equal21Male'}))
    ->setCellValue('E14', number_format($objclass12->{'equal21Female'}))
    ->setCellValue('D15', number_format($objclass12->{'equal22Male'}))
    ->setCellValue('E15', number_format($objclass12->{'equal22Female'}))
    ->setCellValue('D16', number_format($objclass12->{'equal23Male'}))
    ->setCellValue('E16', number_format($objclass12->{'equal23Female'}))
    ->setCellValue('D17', number_format($objclass12->{'equal24Male'}))
    ->setCellValue('E17', number_format($objclass12->{'equal24Female'}))
    ->setCellValue('D18', number_format($objclass12->{'25to29Male'}))
    ->setCellValue('E18', number_format($objclass12->{'25to29Female'}))
    ->setCellValue('D19', number_format($objclass12->{'30to34Male'}))
    ->setCellValue('E19', number_format($objclass12->{'30to34Female'}))
    ->setCellValue('D20', number_format($objclass12->{'35to39Male'}))
    ->setCellValue('E20', number_format($objclass12->{'35to39Female'}))
    ->setCellValue('D21', number_format($objclass12->{'40to44Male'}))
    ->setCellValue('E21', number_format($objclass12->{'40to44Female'}))
    ->setCellValue('D22', number_format($objclass12->{'45to49Male'}))
    ->setCellValue('E22', number_format($objclass12->{'45to49Female'}))
    ->setCellValue('D23', number_format($objclass12->{'more50Male'}))
    ->setCellValue('E23', number_format($objclass12->{'more50Female'}))

    ->setCellValue('F2', 'ม.ปลาย')
    ->setCellValue('F3', 'ชาย')
    ->setCellValue('G3', 'หญิง')
    ->setCellValue('F4', number_format($objclass13->{'agelessMale'}))
    ->setCellValue('G4', number_format($objclass13->{'agelessFemale'}))
    ->setCellValue('F5', number_format($objclass13->{'equal12Male'}))
    ->setCellValue('G5', number_format($objclass13->{'equal12Female'}))
    ->setCellValue('F6', number_format($objclass13->{'equal13Male'}))
    ->setCellValue('G6', number_format($objclass13->{'equal13Female'}))
    ->setCellValue('F7', number_format($objclass13->{'equal14Male'}))
    ->setCellValue('G7', number_format($objclass13->{'equal14Female'}))
    ->setCellValue('F8', number_format($objclass13->{'equal15Male'}))
    ->setCellValue('G8', number_format($objclass13->{'equal15Female'}))
    ->setCellValue('F9', number_format($objclass13->{'equal16Male'}))
    ->setCellValue('G9', number_format($objclass13->{'equal16Female'}))
    ->setCellValue('F10', number_format($objclass13->{'equal17Male'}))
    ->setCellValue('G10', number_format($objclass13->{'equal17Female'}))
    ->setCellValue('F11', number_format($objclass13->{'equal18Male'}))
    ->setCellValue('G11', number_format($objclass13->{'equal18Female'}))
    ->setCellValue('F12', number_format($objclass13->{'equal19Male'}))
    ->setCellValue('G12', number_format($objclass13->{'equal19Female'}))
    ->setCellValue('F13', number_format($objclass13->{'equal20Male'}))
    ->setCellValue('G13', number_format($objclass13->{'equal20Female'}))
    ->setCellValue('F14', number_format($objclass13->{'equal21Male'}))
    ->setCellValue('G14', number_format($objclass13->{'equal21Female'}))
    ->setCellValue('F15', number_format($objclass13->{'equal22Male'}))
    ->setCellValue('G15', number_format($objclass13->{'equal22Female'}))
    ->setCellValue('F16', number_format($objclass13->{'equal23Male'}))
    ->setCellValue('G16', number_format($objclass13->{'equal23Female'}))
    ->setCellValue('F17', number_format($objclass13->{'equal24Male'}))
    ->setCellValue('G17', number_format($objclass13->{'equal24Female'}))
    ->setCellValue('F18', number_format($objclass13->{'25to29Male'}))
    ->setCellValue('G18', number_format($objclass13->{'25to29Female'}))
    ->setCellValue('F19', number_format($objclass13->{'30to34Male'}))
    ->setCellValue('G19', number_format($objclass13->{'30to34Female'}))
    ->setCellValue('F20', number_format($objclass13->{'35to39Male'}))
    ->setCellValue('G20', number_format($objclass13->{'35to39Female'}))
    ->setCellValue('F21', number_format($objclass13->{'40to44Male'}))
    ->setCellValue('G21', number_format($objclass13->{'40to44Female'}))
    ->setCellValue('F22', number_format($objclass13->{'45to49Male'}))
    ->setCellValue('G22', number_format($objclass13->{'45to49Female'}))
    ->setCellValue('F23', number_format($objclass13->{'more50Male'}))
    ->setCellValue('G23', number_format($objclass13->{'more50Female'}))

    ->setCellValue('A24', 'ไม่ระบุ')
    ->setCellValue('B24', number_format($objclass11->{'undifiedmale11'}))
    ->setCellValue('C24', number_format($objclass11->{'undifiedfemale11'}))
    ->setCellValue('D24', number_format($objclass12->{'undifiedmale12'}))
    ->setCellValue('E24', number_format($objclass12->{'undifiedfemale12'}))
    ->setCellValue('F24', number_format($objclass13->{'undifiedmale13'}))
    ->setCellValue('G24', number_format($objclass13->{'undifiedfemale13'}))

    ->setCellValue('A25', 'รวม')
    ->setCellValue('B25', number_format($objclass11->{'totalclass11male'}))
    ->setCellValue('C25', number_format($objclass11->{'totalclass11female'}))
    ->setCellValue('D25', number_format($objclass12->{'totalclass12male'}))
    ->setCellValue('E25', number_format($objclass12->{'totalclass12female'}))
    ->setCellValue('F25', number_format($objclass13->{'totalclass13male'}))
    ->setCellValue('G25', number_format($objclass13->{'totalclass13female'}))

    ->setCellValue('A26', 'รวมตามระดับการศึกษา')
    ->mergeCells('B26:C26')
    ->mergeCells('D26:E26')
    ->mergeCells('F26:G26')
    ->setCellValue('B26', number_format($objclass11->{'totalfinalclass11'}))
    ->setCellValue('D26', number_format($objclass12->{'totalfinalclass12'}))
    ->setCellValue('F26', number_format($objclass13->{'totalfinalclass13'}))

    ->setCellValue('A27', 'รวมทั้งหมด')
    ->setCellValue('G27', number_format($totalAll));

header('Content-Type: application/xls');
header("Content-Disposition: attachment; filename=Report_{$provinceName}.xls");
header("Cache-Control: max-age=0");

// write File
$file = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$file->save('php://output');
