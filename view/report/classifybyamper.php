<html>

<head>
  <?php
  include "../../configs/connection.php";
  include "../../resource.php";
  $connect = OpenCon();
  $term = $_GET['term'];
  $year = $_GET['year'];
  $provinceuid=$_GET['provinceuid'];
  $provincename = $_GET['provincename'];
  $index = 0;
  ?>
  <title>IIS Windows Server</title>
</head>

<body>
    <div>

  <div class="container">
    <div class="col*lg-12">
      <?php
      if ($year && $term) {
        $query = "SELECT * FROM totalstudent{$year}_{$term} ";
        $result = $connect->query($query);
        $num = $result->num_rows;
        // echo "นับได้ ",$num;

        if ($num == 0) {
          echo "<center>กรุณาเลือกข้อมูลใหม่ ไม่มีข้อมูลที่ท่านเลือก!!!</center>";
          exit();
        }
      } else {
        exit();
      }
      ?>

      <a href="/student/view/report/reportstudentage.php?year=<?=$year?>&term=<?= $term ?>" class=""><img src="../../public/home-button.png" title="กลับหน้าหลัก" style="width: 2%;" style=" heigh: 2%;"></a>
      <h3>รายงานข้อมูลนักศึกษาแยกตามระดับชั้น และช่วงอายุ ภาคเรียนที่ <?= $term ?>/<?= $year ?> ของกศน.อำเภอ จังหวัด<?=$provincename?></h3>
  

      <table class="styled-table">
        <thead>
          <tr>
          <th align='center'>ลำดับที่</th>
            <th align='center'>รหัสอำเภอ</th>
            <th align='center'>ศูนย์การศึกษานอกระบบและการศึกษาตามอัธยาศัย</th>
            <th align='center'>ดูรายละเอียดช่วงอายุ</th>
          </tr>
        </thead>
        <?php
        $sql    = "SELECT table1.id, table1.district_id, table1.district_name, table2.province_id, table2.province_name FROM m_district as table1 LEFT JOIN m_province as table2 ON table1.province_id = table2.province_id WHERE table1.province_id='$provinceuid'";
        $result = mysqli_query($connect, $sql);
        while ($row = mysqli_fetch_array($result)) {
          $index+=1;
          $districtuid  = $row['district_id'];
          $districtname = $row['district_name'];

        ?>

          <tr>
            <td align='center'><?= $index ?></td>
            <td align='center'><?= $districtuid ?></td>
            <td><?= $districtname; ?></td>
            <td align='center'> <a href="reportstudentnfe.php?districtuid=<?= $districtuid ?>&districtname=<?=$districtname?>&year=<?= $year ?>&term=<?= $term ?>&provinceuid=<?=$provinceuid?>&provincename=<?=$provincename?>"> คลิกเพื่อดูรายงาน </a></td>
          </tr>

        <?php } ?>
      </table>
    </div>
  </div>
  
  <center>
    <div>
      <hr noshade width=1000 size=1>
      </td>
      </tr>
      <p><b>
          <font color="#424949">ระบบรายงานข้อมูลสารสนเทศ การศึกษานอกระบบและการศึกษาตามอัธยาศัย</font>
      </p></b>
      <p>
        <font color="#515A5A">สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย สำนักงานปลัดกระทรวงศึกษาธิการ</font>
      </p>
    </div>
  </center>
  

</body>

</html>