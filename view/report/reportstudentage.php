<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>IIS Windows Server</title>
  <style>
    table,
    td,
    th {
      border: 1px solid #ddd;

    }

    table {
      border-collapse: collapse;
      width: 100%;
      text-align: left;

    }

    th,
    td {
      padding: 5px;


    }

    .styled-table {
      border-collapse: collapse;
      margin: 3px 0;
      font-size: 16px;
      font-family: sans-serif;
      min-width: 100px;
      box-shadow: 0 0 30px rgba(0, 0, 0, 0.15);
      width: 100%;
    }

    .styled-table thead tr {
      background-color: #009879;
      color: #ffffff;
      text-align: left;
    }

    .styled-table th,
    .styled-table td {
      padding: 14px 12px;
    }

    .styled-table tbody tr {
      border-bottom: 2px solid #dddddd;
    }

    .styled-table tbody tr:nth-of-type(even) {
      background-color: #f3f3f3;
    }

    .styled-table tbody tr:last-of-type {
      border-bottom: 2px solid #009879;
    }

    .styled-table tbody tr.active-row {
      font-weight: bold;
      color: #009879;
    }
    .button {
      display: inline-block;
      padding: 10px 10px;
      height: 50px;
      font-size: 18px;

      cursor: pointer;
      text-align: center;
      text-decoration: 10px;
      outline: 10px;
      color: #fff;
      background-color: #5DADE2;
      border: 1px;
      border-radius: 5px;
      box-shadow: 0 5px #999;
      border-style: dotted;
      border-color: rgba(0, 0, 255, 0.909);

    }

    .button:hover {
      background-color: #6bb76d
    }

    .button:active {
      background-color: #c892eac9;
      box-shadow: 0 5px #666;
      transform: translateY(4px);
    }
  </style>
</head>

<body>
  <?php
include "../../index.php";
include "../../public/banner.php";
include '../../configs/connection.php';
include "../../controllers/queryclassstudenttallthailand.class.php";
$connect = OpenCon();
$term    = $_GET['term'];
$year    = $_GET['year'];
$obj = new QueryAll;
$type            = 'Allthailand';
$year           = $_GET['year'];
$term           = $_GET['term'];
$districtoid    = null;
$subdistrictoid = null;

$dataclass11 = $obj->queryClassStudent11($connect, $year, $term);
$objclass11  = json_decode($dataclass11);

$dataclass12 = $obj->queryClassStudent12($connect, $year, $term);
$objclass12  = json_decode($dataclass12);

$dataclass13 = $obj->queryClassStudent13($connect, $year, $term);
$objclass13  = json_decode($dataclass13);

$totalAll = $objclass11->{'totalfinalclass11'}+$objclass12->{'totalfinalclass12'}+$objclass13->{'totalfinalclass13'};
?>
  <div class="container">
    <div class="col*lg-12">
      <?php
      if ($year && $term) {
        $query = "SELECT * FROM totalstudent{$year}_{$term} ";
        $result = $connect->query($query);
        $num = $result->num_rows;

        if ($num == 0) {
          echo "<center>กรุณาเลือกข้อมูลใหม่ ไม่มีข้อมูลที่ท่านเลือก!!!</center>";
          exit();
        }
      } else {
        exit();
      }
      ?>
      <div class="container">
    <div class="col*lg-12">
      <table class="styled-table">
        <thead>
          <tr>
            <th rowspan="2" style="text-align: center;">ช่วงอายุ</th>
            <th colspan="2" style="text-align: center;">ประถมศึกษา</th>
            <th colspan="2" style="text-align: center;">มัธยมต้น</th>
            <th colspan="2" style="text-align: center;">มัธยมปลาย</th>
          </tr>
          <tr>

            <th style="text-align: center;">ชาย</th>
            <th style="text-align: center;">หญิง</th>
            <th style="text-align: center;">ชาย</th>
            <th style="text-align: center;">หญิง</th>
            <th style="text-align: center;">ชาย</th>
            <th style="text-align: center;">หญิง</th>

          </tr>
        </thead>
        <h3>รายงานสรุปข้อมูลนักศึกษาทั่วประเทศไทย จำแนกตามช่วงอายุ ของภาคเรียนที่ 1/2565  &nbsp;&nbsp;<a href="/student/controllers/exportexcel.php?type=<?=$type?>&year=<?=$year?>&term=<?=$term?>" class="btn btn-primary btn-sm"><img src="../../public/excel.png" title="พิมพ์เอกสาร" style="width: 20;"></img></a> ดาวน์โหลดเอกสาร</h3>
    
    
        <tr>
          <td align='center'><?php echo "น้อยกว่า 12 ปี "; ?></td>
          <td align='center'><?php echo number_format($objclass11->{'agelessMale'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'agelessFemale'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'agelessMale'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'agelessFemale'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'agelessMale'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'agelessFemale'}); ?></td>
        </tr>

        <tr>
         
         <td align='center'>12 ปี </td>
         <td align='center'><?php echo number_format($objclass11->{'equal12Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass11->{'equal12Female'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'equal12Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'equal12Female'}); ?></td>
         <td align='center'><?php echo number_format($objclass13->{'equal12Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass13->{'equal12Female'}); ?></td>
       </tr>

       <tr>
         
         <td align='center'>13 ปี</td>
         <td align='center'><?php echo number_format($objclass11->{'equal13Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass11->{'equal13Female'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'equal13Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'equal13Female'}); ?></td>
         <td align='center'><?php echo number_format($objclass13->{'equal13Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass13->{'equal13Female'}); ?></td>
       </tr>

       <tr>
         
         <td align='center'>14 ปี</td>
         <td align='center'><?php echo number_format($objclass11->{'equal14Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass11->{'equal14Female'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'equal14Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'equal14Female'}); ?></td>
         <td align='center'><?php echo number_format($objclass13->{'equal14Male'}); ?></td>
         <td align='center'><?php echo number_format($objclass13->{'equal14Female'}); ?></td>
       </tr>

        <tr>
         
          <td align='center'>15 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal15Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal15Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal15Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal15Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal15Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal15Female'}); ?></td>
        </tr>

        <tr>
         
          <td align='center'>16 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal16Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal16Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal16Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal16Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal16Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal16Female'}); ?></td>
        </tr>

        <tr>
          
          <td align='center'>17 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal17Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal17Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal17Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal17Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal17Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal17Female'}); ?></td>
        </tr>

        <tr>
        
          <td align='center'>18 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal18Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal18Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal18Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal18Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal18Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal18Female'}); ?></td>
        </tr>

        <tr>
        
          <td align='center'>19 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal19Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal19Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal19Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal19Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal19Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal19Female'}); ?></td>
        </tr>

        <tr>
          
          <td align='center'>20 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal20Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal20Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal20Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal20Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal20Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal20Female'}); ?></td>
        </tr>

        <tr>
     
          <td align='center'>21 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal21Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal21Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal21Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal21Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal21Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal21Female'}); ?></td>
        </tr>

        <tr>
      
          <td align='center'>22 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal22Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal22Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal22Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal22Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal22Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal22Female'}); ?></td>  
        </tr>

        <tr>
          
          <td align='center'>23 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal23Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal23Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal23Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal23Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal23Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal23Female'}); ?></td> 
        </tr>

        <tr>
        
          <td align='center'>24 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal24Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal24Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal24Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal24Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal24Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal24Female'}); ?></td>
        </tr>

        <tr>
         
          <td align='center'>25 - 29 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'25to29Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'25to29Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'25to29Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'25to29Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'25to29Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'25to29Female'}); ?></td>

        </tr>

        <tr>
          
          <td align='center'>30 - 34 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'30to34Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'30to34Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'30to34Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'30to34Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'30to34Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'30to34Female'}); ?></td>
        </tr>

        <tr>
          
          <td align='center'>35 - 39 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'35to39Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'35to39Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'35to39Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'35to39Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'35to39Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'35to39Female'}); ?></td>
        </tr>

        <tr>
          
          <td align='center'>40 - 44 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'40to44Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'40to44Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'40to44Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'40to44Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'40to44Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'40to44Female'}); ?></td>
        </tr>

        <tr>
         
          <td align='center'>45 - 49 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'45to49Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'45to49Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'45to49Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'45to49Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'45to49Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'45to49Female'}); ?></td>
        </tr>

        <tr>
         
          <td align='center'>50 - 60 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'more50Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'more50Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'more50Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'more50Female'}); ?></td> 
          <td align='center'><?php echo number_format($objclass13->{'more50Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'more50Female'}); ?></td> 
        </tr>

        <tr>
         <td align='center'> ไม่ระบุ </td>
         <td align='center'><?php echo number_format($objclass11->{'undifiedmale11'}); ?></td>
         <td align='center'><?php echo number_format($objclass11->{'undifiedfemale11'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'undifiedmale12'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'undifiedfemale12'}); ?></td> 
         <td align='center'><?php echo number_format($objclass13->{'undifiedmale13'}); ?></td>
         <td align='center'><?php echo number_format( $objclass13->{'undifiedfemale13'}); ?></td> 
       </tr>
       
       <tr>
         <td align='center'><b> รวม <b></td>
         <td align='center'><?php echo number_format($objclass11->{'totalclass11male'}); ?></td>
         <td align='center'><?php echo number_format($objclass11->{'totalclass11female'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'totalclass12male'}); ?></td>
         <td align='center'><?php echo number_format($objclass12->{'totalclass12female'}); ?></td> 
         <td align='center'><?php echo number_format($objclass13->{'totalclass13male'}); ?></td>
         <td align='center'><?php echo number_format($objclass13->{'totalclass13female'}); ?></td> 
       </tr>

       <tr>
         <td align='center'><b> รวมตามระดับการศึกษา</b> </td>
         <td></td>
         <td align='center'><?php echo number_format($objclass11->{'totalfinalclass11'}); ?>
         <td></td>
         <td align='center'><?php echo number_format($objclass12->{'totalfinalclass12'}); ?></td>
         <td></td>
         <td align='center'><?php echo number_format($objclass13->{'totalfinalclass13'}); ?></td>
       </tr>

       <tr>
         <td align='center'><b> รวมทั้งหมด</b> </td>
         <td></td>  <td></td>  <td></td> <td></td>  <td></td>
         <td align='center'><?php echo number_format($totalAll); ?></td>
       </tr>

        <?php

        ?>
      </table><br>
    </div>
    <br>
  </div>
  <h3>รายงานข้อมูลนักศึกษาแยกตามระดับชั้น และช่วงอายุ ภาคเรียนที่ <?= $term ?>/<?= $year ?> จำแนกรายพื้นที่</h3>
      <table class="styled-table">
        <thead>
          <tr>
            <th align='center'>ลำดับที่</th>
            <th align='center'>ศูนย์การศึกษานอกระบบและการศึกษาตามอัธยาศัย</th>
            <th align='center'>ระดับตำบล</th>
            <th align='center'>ระดับอำเภอ</th>
            <th align='center'>ระดับจังหวัด</th>
          </tr>
        </thead>
        <?php
        $sql    = "SELECT * FROM `m_province` ";
        $result = mysqli_query($connect, $sql);
        while ($row = mysqli_fetch_array($result)) {
          $index += 1;
          $id           = $row['oid'];
          $provinceuid  = $row['province_id'];
          $provincename = $row['province_name'];

        ?>

          <tr>
            <td align='center'><?= $index ?></td>

            <!-- <td align='center'><?= $provinceuid; ?></td> -->
            <td><?= $provincename; ?></td>

            <?php
            if ($provinceuid != '1210020000' && $provinceuid != '1210370002') {
            ?>
              <td align='center'> <a href="classifybytumbon.php?provinceuid=<?= $provinceuid ?>&provincename=<?= $provincename ?>&year=<?= $year ?>&term=<?= $term ?>" target="_blank"><img src="../../public/folder.png" title="คลิกเพื่อดูรายงานของกศน.ตำบล" style="width: 20;"></img></a></td>
            <?php
            } else {
            ?>
            <?php
            }
            ?>


            <?php
            if ($provinceuid != '1210020000' && $provinceuid != '1210370002') {
            ?>
              <td align='center'> <a href="classifybyamper.php?provinceuid=<?= $provinceuid ?>&provincename=<?= $provincename ?>&year=<?= $year ?>&term=<?= $term ?>" target="_blank"> <img src="../../public/folder.png" title="คลิกเพื่อดูรายงานของกศน.อำเภอ" style="width: 20;"></img></a></td>
            <?php
            } else {
            ?>
              <td></td>


            <?php
            }
            ?>

            <?php
            if ($provinceuid != '1210020000' && $provinceuid != '1210370002') {
            ?>
              <td align='center'> <a href="reportstudentnfe.php?provinceuid=<?= $provinceuid ?>&provincename=<?= $provincename ?>&year=<?= $year ?>&term=<?= $term ?>" target="_blank"><img src="../../public/folder.png" title="คลิกเพื่อดูรายงานของกศน.จังหวัด" style="width: 20;"></img></a></td>
            <?php
            } else {
            ?>
              <td align='center'> <a href="reportstudentnfe.php?provinceuid=<?= $provinceuid ?>&provincename=<?= $provincename ?>&year=<?= $year ?>&term=<?= $term ?>" target="_blank"><img src="../../public/folder.png" title="คลิกเพื่อดูรายงานของกศน.ตำบล" style="width: 20;"></img></a></td>
            <?php
            }
            ?>
          </tr>

        <?php } ?>

      </table>
    </div>
  </div>

  <center>
    <div>
      <hr noshade width=1000 size=1>
      </td>
      </tr>
      <p><b>
          <font color="#424949">ระบบรายงานข้อมูลสารสนเทศ การศึกษานอกระบบและการศึกษาตามอัธยาศัย</font>
      </p></b>
      <p>
        <font color="#515A5A">สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย สำนักงานปลัดกระทรวงศึกษาธิการ</font>
      </p>
    </div>
  </center>
</body>

</html>