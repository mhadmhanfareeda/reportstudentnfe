<!DOCTYPE html>
<html>

<head>
  <?php
        include "../../configs/connection.php";
        include "../../controllers/querystudent.class.php";
        include "../../resource.php";

        $connect = OpenCon();
        $obj = new Query;
        $year = $_GET['year'] ? $_GET['year'] : null;
        $term = $_GET['term'] ? $_GET['term'] : null;
        $districtuid = $_GET['districtuid'] ? $_GET['districtuid'] : null;
        $districtname = $_GET['districtname'] ? $_GET['districtname'] : null;
        $subdistrictuid = $_GET['subdistrictuid'] ? $_GET['subdistrictuid'] : null;
        $subdistrictname = $_GET['subdistrictname'] ? $_GET['subdistrictname'] : null;
        $provinceuid = $_GET['provinceuid'] ? $_GET['provinceuid'] : null;
        $provincename = $_GET['provincename'] ? $_GET['provincename'] : null;
        $pathback = '';
        $con='';

        $dataclass11 = $obj->queryClassStudent11($provinceuid, $connect, $year, $term, $districtuid, $subdistrictuid);
        $objclass11 = json_decode($dataclass11);

        $dataclass12 = $obj->queryClassStudent12($provinceuid, $connect, $year, $term, $districtuid, $subdistrictuid);
        $objclass12 = json_decode($dataclass12);

        $dataclass13 = $obj->queryClassStudent13($provinceuid, $connect, $year, $term, $districtuid, $subdistrictuid);
        $objclass13 = json_decode($dataclass13);

        $totalAll = $objclass11->{'totalfinalclass11'} + $objclass12->{'totalfinalclass12'} + $objclass13->{'totalfinalclass13'};

        if($districtuid && $districtname){
          $pathback = 'classifybyamper';
          $con = 'provinceuid='.$provinceuid.'&provincename='.$provincename;
        }else if($subdistrictuid && $subdistrictname){
          $pathback = 'classifybytumbon';
          $con = 'provinceuid='.$provinceuid.'&provincename='.$provincename;
        }else{
          $pathback = 'reportstudentage';
        }

        ?>
  <title>nfe_school</title>
</head>

<body>

  <div class="container">
    <div class="col*lg-12">
      <br>
      <div>
        <a href="/student/view/report/reportstudentage.php?year=<?= $year ?>&term=<?= $term ?>"><img src="../../public/home-button.png" title="กลับหน้าหลัก" style="width:2%;" style=" heigh: 2%;"></img></a>&nbsp;&nbsp;

        <a href="/student/view/report/<?=$pathback?>.php?<?=$con?>&year=<?= $year ?>&term=<?= $term ?>"><img src="../../public/previous.png" title="ย้อนกลับ" style="width:2%;" style=" heigh: 2%;"></img></a>
      </div>
      <table class="styled-table">
        <thead>
          <tr>
            <th rowspan="2">ช่วงอายุ</th>
            <th colspan="2">ประถมศึกษา</th>
            <th colspan="2">มัธยมต้น</th>
            <th colspan="2">มัธยมปลาย</th>
          </tr>
          <tr>
            <th>ชาย</th>
            <th>หญิง</th>
            <th>ชาย</th>
            <th>หญิง</th>
            <th>ชาย</th>
            <th>หญิง</th>

          </tr>
        </thead>
        <?php
        if ($districtname && $districtuid) {
        ?>

          <h3>รายงานข้อมูลนักศึกษาของสำนักงาน กศน.อำเภอ<?= $districtname; ?> จำแนกตามช่วงอายุ &nbsp;<a href="../../controllers/exportexcel.php?&year=<?= $year ?>&provinceuid=<?= $provinceuid ?>&term=<?= $term ?>&districtuid=<?= $districtuid ?>&districtname=<?= $districtname ?>"><img src="../../public/excel.png" title="พิมพ์รายงาน" style="width: 2%;" style=" heigh: 2%;"></a></h3>
        <?php
        } else if ($subdistrictname && $subdistrictuid) {
        ?>

          <h3>รายงานข้อมูลนักศึกษาของสำนักงาน กศน.ตำบล<?= $subdistrictname; ?> จำแนกตามช่วงอายุ &nbsp; <a href="../../controllers/exportexcel.php?year=<?= $year ?>&provinceuid=<?= $provinceuid ?>&term=<?= $term ?>&subdistrictuid=<?= $subdistrictuid ?>&subdistrictname=<?= $subdistrictname ?>"><img src="../../public/excel.png" title="พิมพ์รายงาน" style="width: 2%;" style=" heigh: 2%;"></a></h3>

        <?php
        } else {
        ?>

          <h3>รายงานข้อมูลนักศึกษาของสำนักงาน กศน.จังหวัด<?= $provincename; ?> จำแนกตามช่วงอายุ &nbsp;<a href="../../controllers/exportexcel.php?provincename=<?= $provincename ?>&year=<?= $year ?>&provinceuid=<?= $provinceuid ?>&term=<?= $term ?>"><img src="../../public/excel.png" title="พิมพ์รายงาน" style="width: 2%;" style=" heigh: 2%;"></a></h3>

        <?php
        }
        ?>
        <?php
        ?>


        <tr>
          <td align='center'><?php echo "น้อยกว่า 12 ปี "; ?></td>
          <td align='center'><?php echo number_format($objclass11->{'agelessMale'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'agelessFemale'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'agelessMale'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'agelessFemale'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'agelessMale'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'agelessFemale'}); ?></td>
        </tr>

        <tr>

          <td align='center'>12 ปี </td>
          <td align='center'><?php echo number_format($objclass11->{'equal12Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal12Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal12Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal12Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal12Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal12Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>13 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal13Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal13Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal13Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal13Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal13Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal13Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>14 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal14Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal14Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal14Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal14Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal14Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal14Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>15 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal15Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal15Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal15Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal15Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal15Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal15Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>16 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal16Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal16Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal16Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal16Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal16Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal16Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>17 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal17Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal17Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal17Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal17Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal17Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal17Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>18 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal18Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal18Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal18Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal18Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal18Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal18Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>19 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal19Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal19Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal19Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal19Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal19Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal19Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>20 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal20Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal20Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal20Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal20Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal20Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal20Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>21 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal21Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal21Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal21Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal21Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal21Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal21Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>22 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal22Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal22Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal22Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal22Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal22Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal22Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>23 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal23Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal23Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal23Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal23Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal23Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal23Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>24 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'equal24Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'equal24Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal24Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'equal24Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal24Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'equal24Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>25 - 29 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'25to29Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'25to29Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'25to29Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'25to29Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'25to29Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'25to29Female'}); ?></td>

        </tr>

        <tr>

          <td align='center'>30 - 34 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'30to34Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'30to34Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'30to34Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'30to34Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'30to34Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'30to34Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>35 - 39 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'35to39Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'35to39Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'35to39Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'35to39Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'35to39Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'35to39Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>40 - 44 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'40to44Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'40to44Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'40to44Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'40to44Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'40to44Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'40to44Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>45 - 49 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'45to49Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'45to49Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'45to49Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'45to49Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'45to49Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'45to49Female'}); ?></td>
        </tr>

        <tr>

          <td align='center'>50 - 60 ปี</td>
          <td align='center'><?php echo number_format($objclass11->{'more50Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'more50Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'more50Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'more50Female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'more50Male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'more50Female'}); ?></td>
        </tr>

        <tr>
          <td align='center'> ไม่ระบุ </td>
          <td align='center'><?php echo number_format($objclass11->{'undifiedmale11'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'undifiedfemale11'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'undifiedmale12'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'undifiedfemale12'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'undifiedmale13'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'undifiedfemale13'}); ?></td>
        </tr>

        <tr>
          <td align='center'><b> รวม <b> </td>
          <td align='center'><?php echo number_format($objclass11->{'totalclass11male'}); ?></td>
          <td align='center'><?php echo number_format($objclass11->{'totalclass11female'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'totalclass12male'}); ?></td>
          <td align='center'><?php echo number_format($objclass12->{'totalclass12female'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'totalclass13male'}); ?></td>
          <td align='center'><?php echo number_format($objclass13->{'totalclass13female'}); ?></td>
        </tr>

        <tr>
          <td align='center'><b>รวมตามระดับการศึกษา<b></td>
          <td></td>
          <td align='center'><?php echo number_format($objclass11->{'totalfinalclass11'}); ?>
          <td></td>
          <td align='center'><?php echo number_format($objclass12->{'totalfinalclass12'}); ?></td>
          <td></td>
          <td align='center'><?php echo number_format($objclass13->{'totalfinalclass13'}); ?></td>
        </tr>

        <tr>
          <td align='center'><b>รวมทั้งหมด<b></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align='center'><?php echo number_format($totalAll); ?></td>
        </tr>

        <?php

        ?>
      </table>

    </div>
    <br>
  </div>
</body>

</html>