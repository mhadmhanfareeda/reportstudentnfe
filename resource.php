<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<style>
    table,
    td,
    th {
      border: 1px solid #ddd;
    }
      th {
         text-align: center; 
    }

    table {
      border-collapse: collapse;
      width: 100%;
      text-align: left;

    }

    th,
    td {
      padding: 3px;


    }

    .styled-table {
      border-collapse: collapse;
      margin: 3px 0;
      font-size: 16px;
      font-family: "sans-serif";
      min-width: 100px;
      box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
      width: 100%;
    }

    .styled-table thead tr {
      background-color: #009879;
      color: #ffffff;
      text-align: left;
    }

    .styled-table th,
    .styled-table td {
      padding: 8px 10px;
    }

    .styled-table tbody tr {
      border-bottom: 1px solid #dddddd;
    }

    .styled-table tbody tr:nth-of-type(even) {
      background-color: #f3f3f3;
    }

    .styled-table tbody tr:last-of-type {
      border-bottom: 2px solid #009879;
    }

    .styled-table tbody tr.active-row {
      font-weight: bold;
      color: #009879;
    }
  </style>